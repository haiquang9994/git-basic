# Hướng dẫn sử dụng git

## Khởi tạo git
```
git init
```
---
## Thêm remote của cloud
```
git remote add [name] [url]
```
---
## Add file

### Add tất cả file (thay đổi file, file mới, xóa file) trong repository
```
git add -A

```

### Add tất cả file trong một folder
```
git add [path]
```

### Bỏ một file đã được add
```
git reset [file]
```
---

## Commit

### Commit các file đã add trước đó
```
git commit -m [comment]
```

### Add và commit các file được thay đổi
```
git commit -am [comment]
```

### Thay đổi comment của commit cuối cùng
```
git commit --amend -m [new_comment]
```

---
## Checkout
### Checkout qua một nhánh khác trên local
```
git checkout [branch]
```

### Checkout qua một nhánh mới từ nhánh hiện tại trên local
```
git checkout -b [branch]
```
---

## Pull
### Pull code từ một nhánh trên cloud vào nhánh hiện tại
```
git pull [remote] [branch]
```

---
## Merge
### Merge code từ một nhánh trên local vào nhánh hiện tại
```
git merge [branch]
```

---
## Push
### Push code lên một nhánh trên cloud
```
git push [remote] [branch]
```
Lưu ý: Không cần checkout qua nhánh cần push

---

